#ifndef NPARSER_NPARSERTYPE_PROTECT
#define NPARSER_NPARSERTYPE_PROTECT

// -------------------------
// enum NParser::NParserType
// -------------------------

typedef enum NParserType
{
	NPARSER_TYPE_YAML,
	NPARSER_TYPE_JSON,

	NPARSER_TYPES
} NParserType;

#endif // !NPARSER_NPARSERTYPE_PROTECT
