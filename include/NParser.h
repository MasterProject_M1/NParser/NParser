#ifndef NPARSER_PROTECT
#define NPARSER_PROTECT

// -----------------
// namespace NParser
// -----------------

// namespace NLib
#include "../../../NLib/NLib/NLib/include/NLib/NLib.h"

// namespace NParser::Output
#include "Output/NParser_Output.h"

// enum NParser::NParserType
#include "NParser_NParserType.h"

// namespace NYaml
#include "../../NYaml/include/NYaml.h"

// namespace NJson
#include "../../NJson/include/NJson.h"

#ifdef NPARSER_EASY_FUNCTION
/**
 * Parse file
 *
 * @param filepath
 * 		The file path
 * @param type
 * 		The file type
 *
 * @return the parsed result
 */
__ALLOC NParserOutputList *NParser_ParseFile( const char *filepath,
	NParserType type );

/**
 * Parse text
 *
 * @param text
 * 		The text
 * @param type
 * 		The text type
 *
 * @return the parsed result
 */
__ALLOC NParserOutputList *NParser_ParseText( const char *text,
	NParserType type );
#endif // NPARSER_EASY_FUNCTION

#endif // !NPARSER_PROTECT

