#ifndef NPARSER_OUTPUT_NPARSEROUTPUTTYPE_PROTECT
#define NPARSER_OUTPUT_NPARSEROUTPUTTYPE_PROTECT

// ---------------------------------------
// enum NParser::Output::NParserOutputType
// ---------------------------------------

typedef enum NParserOutputType
{
	NPARSER_OUTPUT_TYPE_STRING,
	NPARSER_OUTPUT_TYPE_INTEGER,
	NPARSER_OUTPUT_TYPE_DOUBLE,
	NPARSER_OUTPUT_TYPE_BOOLEAN,

	NPARSER_OUTPUT_TYPES
} NParserOutputType;

/**
 * Get output type name
 *
 * @param type
 * 		The parser output type
 *
 * @return the type name
 */
const char *NParser_Output_NParserOutputType_GetTypeName( NParserOutputType type );

#ifdef NPARSER_OUTPUT_NPARSEROUTPUTTYPE_INTERNE
static const char NParserOutputTypeName[ NPARSER_OUTPUT_TYPES ][ 32 ] =
{
	"String",
	"Integer",
	"Double",
	"Boolean"
};
#endif // NPARSER_OUTPUT_NPARSEROUTPUTTYPE_INTERNE

#endif // !NPARSER_OUTPUT_NPARSEROUTPUTTYPE_PROTECT

