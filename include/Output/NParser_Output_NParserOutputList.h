#ifndef NPARSER_OUTPUT_NPARSEROUTPUTLIST_PROTECT
#define NPARSER_OUTPUT_NPARSEROUTPUTLIST_PROTECT

// -----------------------------------------
// struct NParser::Output::NParserOutputList
// -----------------------------------------

typedef struct NParserOutputList
{
	// List (NParserOutput*)
	NListe *m_list;
} NParserOutputList;

/**
 * Build output list
 *
 * @return the output list instance
 */
__ALLOC NParserOutputList *NParser_Output_NParserOutputList_Build( void );

/**
 * Duplicate output list
 *
 * @param src
 * 		The original output list
 *
 * @return the output list instance
 */
__ALLOC NParserOutputList *NParser_Output_NParserOutputList_Build2( const NParserOutputList *src );
/**
 * Destroy output list
 *
 * @param this
 * 		This instance
 */
void NParser_Output_NParserOutputList_Destroy( NParserOutputList ** );

/**
 * Add an entry
 *
 * @param this
 * 		This instance
 * @param key
 * 		The key
 * @param value
 * 		The value
 * @param type
 * 		The value type
 *
 * @return if operation succeeded
 */
NBOOL NParser_Output_NParserOutputList_AddEntry( NParserOutputList*,
	const char *key,
	__WILLBEOWNED char *value,
	NParserOutputType type );

/**
 * Add an entry
 *
 * @param this
 * 		This instance
 * @param entry
 * 		The entry to add
 *
 * @return if operation succeeded
 */
NBOOL NParser_Output_NParserOutputList_AddEntry2( NParserOutputList*,
	__WILLBEOWNED NParserOutput *entry );

/**
 * Add an entry duplicate
 *
 * @param this
 * 		This instance
 * @param entry
 * 		The entry source to duplicate and to add
 *
 * @return if operation succeeded
 */
NBOOL NParser_Output_NParserOutputList_AddEntry3( NParserOutputList*,
	const NParserOutput *entry );

/**
 * Add integer entry
 *
 * @param this
 * 		This instance
 * @param key
 * 		The key
 * @param value
 * 		The value
 *
 * @return if operation succeeded
 */
NBOOL NParser_Output_NParserOutputList_AddEntryInteger( NParserOutputList*,
	const char *key,
	NU32 value );

/**
 * Add boolean value
 *
 * @param this
 * 		This instance
 * @param key
 * 		The key
 * @param value
 * 		The value
 *
 * @return if operation succeeded
 */
NBOOL NParser_Output_NParserOutputList_AddEntryBoolean( NParserOutputList*,
	const char *key,
	NBOOL value );

/**
 * Add double value
 *
 * @param this
 * 		This instance
 * @param key
 * 		The key
 * @param value
 * 		The value
 *
 * @return if operation succeeded
 */
NBOOL NParser_Output_NParserOutputList_AddEntryDouble( NParserOutputList*,
	const char *key,
	double value );

/**
 * Add string value
 *
 * @param this
 * 		This instance
 * @param key
 * 		The key
 * @param value
 * 		The value
 *
 * @return if operations succeeded
 */
NBOOL NParser_Output_NParserOutputList_AddEntryString( NParserOutputList*,
	const char *key,
	const char *value );

/**
 * Add all the entry from another parser output list
 *
 * @param this
 * 		This instance
 * @param parserOutputList
 * 		The other parser output list
 *
 * @return if operation succeeded
 */
NBOOL NParser_Output_NParserOutputList_AddAllEntry( NParserOutputList*,
	NParserOutputList *parserOutputList );

/**
 * Get output by the key
 *
 * @param this
 * 		This instance
 * @param key
 * 		The key to look for
 * @param isKeyCaseSensitive
 * 		Is case sensitive
 * @param isFullKey
 *		Do we want the output key to be exactly %key%, or just to contain it?
 *
 * @return the output
 */
const NParserOutput *NParser_Output_NParserOutputList_GetFirstOutput( const NParserOutputList *this,
	const char *key,
	NBOOL isKeyCaseSensitive,
	NBOOL isFullKey );

/**
 * Get output list by the key
 *
 * @param this
 * 		This instance
 * @param key
 * 		The key to look for
 * @param isKeyCaseSensitive
 * 		Is case sensitive
 * @param isFullKey
 * 		Do we want the output key to be exactly %key%, or just to contain it?
 *
 * @return the output list
 */
__ALLOC NParserOutputList *NParser_Output_NParserOutputList_GetAllOutput( const NParserOutputList *this,
	const char *key,
	NBOOL isKeyCaseSensitive,
	NBOOL isFullKey );

/**
 * Display all output
 *
 * @param this
 * 		This instance
 */
void NParser_Output_NParserOutputList_Display( const NParserOutputList* );

/**
 * Get entry count
 *
 * @param this
 * 		This instance
 *
 * @return the entry count
 */
NU32 NParser_Output_NParserOutputList_GetEntryCount( const NParserOutputList* );

/**
 * Get entry by index
 *
 * @param this
 * 		This instance
 * @param index
 * 		The index
 *
 * @return the entry
 */
const NParserOutput *NParser_Output_NParserOutputList_GetEntry( const NParserOutputList*,
	NU32 index );

/**
 * Remove an entry
 *
 * @param this
 * 		This instance
 * @param index
 * 		The index of entry to remove
 *
 * @return if operation succeeded
 */
NBOOL NParser_Output_NParserOutputList_RemoveEntry( NParserOutputList*,
	NU32 index );

/**
 * Count key occurence
 *
 * @param this
 * 		This instance
 * @param key
 * 		The key to find
 * @param isStartingWith
 * 		Do we check for the key beginning or for all the key
 *
 * @return the occurence count
 */
NBOOL NParser_Output_NParserOutputList_CountKeyOccurence( const NParserOutputList *this,
	const char *key,
	NBOOL isStartingWith );

#endif // !NPARSER_OUTPUT_NPARSEROUTPUTLIST_PROTECT
