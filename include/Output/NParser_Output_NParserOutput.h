#ifndef NPARSER_OUTPUT_NPARSEROUTPUT_PROTECT
#define NPARSER_OUTPUT_NPARSEROUTPUT_PROTECT

// -------------------------------------
// struct NParser::Output::NParserOutput
// -------------------------------------

typedef struct NParserOutput
{
	// Key
	char *m_key;

	// Value type
	NParserOutputType m_type;

	// Value
	char *m_value;
} NParserOutput;

/**
 * Build output
 *
 * @param key
 * 		The key
 * @param value
 * 		The value
 * @param type
 * 		The value type
 *
 * @return the built instance
 */
__ALLOC NParserOutput *NParser_Output_NParserOutput_Build( const char *key,
	__WILLBEOWNED char *value,
	NParserOutputType type );

/**
 * Duplicate entry
 *
 * @param src
 * 		The entry to duplicate from
 *
 * @return the built instance
 */
__ALLOC NParserOutput *NParser_Output_NParserOutput_Build2( const NParserOutput *src );

/**
 * Destroy output
 *
 * @param this
 * 		This instance
 */
void NParser_Output_NParserOutput_Destroy( NParserOutput** );

/**
 * Get key
 *
 * @param this
 * 		This instance
 *
 * @return the key
 */
const char *NParser_Output_NParserOutput_GetKey( const NParserOutput * );

/**
 * Get value
 *
 * @param this
 * 		This instance
 *
 * @return the value
 */
const char *NParser_Output_NParserOutput_GetValue( const NParserOutput * );

/**
 * Get a value copy
 *
 * @param this
 * 		This instance
 *
 * @return the value copy
 */
__ALLOC char *NParser_Output_NParserOutput_GetValueCopy( const NParserOutput* );

/**
 * Get type
 *
 * @param this
 * 		This instance
 *
 * @return the type
 */
NParserOutputType NParser_Output_NParserOutput_GetType( const NParserOutput * );

/**
 * Display instance
 *
 * @param this
 *		This instance
 */
void NParser_Output_NParserOutput_Display( const NParserOutput* );

#endif // !NPARSER_OUTPUT_NPARSEROUTPUT_PROTECT

