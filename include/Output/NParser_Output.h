#ifndef NPARSER_OUTPUT_PROTECT
#define NPARSER_OUTPUT_PROTECT

// -------------------------
// namespace NParser::Output
// -------------------------

// enum NParser::Output::NParserOutputType
#include "NParser_Output_NParserOutputType.h"

// struct NParser::Output::NParserOutput
#include "NParser_Output_NParserOutput.h"

// struct NParser::Output::NParserOutputList
#include "NParser_Output_NParserOutputList.h"

#endif // !NPARSER_OUTPUT_PROTECT

