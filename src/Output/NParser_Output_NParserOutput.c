#include "../../include/NParser.h"

// -------------------------------------
// struct NParser::Output::NParserOutput
// -------------------------------------

/**
 * Build output
 *
 * @param key
 * 		The key
 * @param value
 * 		The value
 * @param type
 * 		The value type
 *
 * @return the built instance
 */
__ALLOC NParserOutput *NParser_Output_NParserOutput_Build( const char *key,
	__WILLBEOWNED char *value,
	NParserOutputType type )
{
	// Output
	__OUTPUT NParserOutput *out;

	// Allocate memory
	if( !( out = calloc( 1,
		sizeof( NParserOutput ) ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Free
		NFREE( value );

		// Quit
		return NULL;
	}

	// Duplicate
	if( !( out->m_key = NLib_Chaine_Dupliquer( key ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Free
		NFREE( value );
		NFREE( out );

		// Quit
		return NULL;
	}

	// Save
	out->m_type = type;
	out->m_value = value;

	// OK
	return out;
}

/**
 * Duplicate entry
 *
 * @param src
 * 		The entry to duplicate from
 *
 * @return the built instance
 */
__ALLOC NParserOutput *NParser_Output_NParserOutput_Build2( const NParserOutput *src )
{
	// Output
	NParserOutput *out;

	// Value
	char *value;

	// Duplicate value
	if( !( value = NParser_Output_NParserOutput_GetValueCopy( src ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_COPY );

		// Quit
		return NULL;
	}

	// Duplicate
	if( !( out = NParser_Output_NParserOutput_Build( src->m_key,
		value,
		src->m_type ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Quit
		return NULL;
	}

	// OK
	return out;
}

/**
 * Destroy output
 *
 * @param this
 * 		This instance
 */
void NParser_Output_NParserOutput_Destroy( NParserOutput **this )
{
	// Free
	NFREE( (*this)->m_key );
	NFREE( (*this)->m_value );
	NFREE( (*this) );
}

/**
 * Get key
 *
 * @param this
 * 		This instance
 *
 * @return the key
 */
const char *NParser_Output_NParserOutput_GetKey( const NParserOutput *this )
{
	return this->m_key;
}

/**
 * Get value
 *
 * @param this
 * 		This instance
 *
 * @return the value
 */
const char *NParser_Output_NParserOutput_GetValue( const NParserOutput *this )
{
	return this->m_value;
}

/**
 * Get a value copy
 *
 * @param this
 * 		This instance
 *
 * @return the value copy
 */
__ALLOC char *NParser_Output_NParserOutput_GetValueCopy( const NParserOutput *this )
{
	// Output
	__OUTPUT char *out = NULL;

	// Size to allocate
	NU32 sizeToAllocate = 0;

	// Find allocate size
	switch( this->m_type )
	{
		case NPARSER_OUTPUT_TYPE_BOOLEAN:
			sizeToAllocate = sizeof( NBOOL );
			break;
		case NPARSER_OUTPUT_TYPE_DOUBLE:
			sizeToAllocate = sizeof( double );
			break;
		case NPARSER_OUTPUT_TYPE_INTEGER:
			sizeToAllocate = sizeof( NS32 );
			break;
		case NPARSER_OUTPUT_TYPE_STRING:
			sizeToAllocate = (NU32)strlen( this->m_value ) + 1;
			break;

		default:
			break;
	}

	// Allocate memory
	if( !( out = calloc( sizeToAllocate,
		sizeof( char ) ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quit
		return NULL;
	}

	// Copy data
	memcpy( out,
		this->m_value,
		sizeToAllocate - ( this->m_type == NPARSER_OUTPUT_TYPE_STRING ?
			1
			: 0 ) );

	// OK
	return out;
}

/**
 * Get type
 *
 * @param this
 * 		This instance
 *
 * @return the type
 */
NParserOutputType NParser_Output_NParserOutput_GetType( const NParserOutput *this )
{
	return this->m_type;
}

/**
 * Display instance
 *
 * @param this
 * 		This instance
 */
void NParser_Output_NParserOutput_Display( const NParserOutput *this )
{
	// Display key and type
	printf( "[%s][%s]=[",
		this->m_key,
		NParser_Output_NParserOutputType_GetTypeName( this->m_type ) );

	// Analyse output type
	switch( this->m_type )
	{
		case NPARSER_OUTPUT_TYPE_BOOLEAN:
			printf( "%s",
				*( (NBOOL*)this->m_value ) ?
				"True"
				: "False" );
			break;
		case NPARSER_OUTPUT_TYPE_DOUBLE:
			printf( "%1.2f",
				*( (double*)this->m_value ) );
			break;
		case NPARSER_OUTPUT_TYPE_INTEGER:
			printf( "%d",
				*( (NS32*)this->m_value ) );
			break;
		case NPARSER_OUTPUT_TYPE_STRING:
			printf( "%s",
				this->m_value );
			break;

		default:
			break;
	}

	puts( "]" );
}

