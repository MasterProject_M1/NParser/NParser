#define NPARSER_OUTPUT_NPARSEROUTPUTTYPE_INTERNE
#include "../../include/NParser.h"

// ---------------------------------------
// enum NParser::Output::NParserOutputType
// ---------------------------------------

/**
 * Get output type name
 *
 * @param type
 * 		The parser output type
 *
 * @return the type name
 */
const char *NParser_Output_NParserOutputType_GetTypeName( NParserOutputType type )
{
	return NParserOutputTypeName[ type ];
}

