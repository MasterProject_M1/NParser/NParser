#include "../../include/NParser.h"

// -----------------------------------------
// struct NParser::Output::NParserOutputList
// -----------------------------------------

/**
 * Build output list
 *
 * @return the output list instance
 */
__ALLOC NParserOutputList *NParser_Output_NParserOutputList_Build( void )
{
	// Output
	__OUTPUT NParserOutputList *out;

	// Allocate memory
	if( !( out = calloc( 1,
		sizeof( NParserOutputList ) ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quit
		return NULL;
	}

	// Create list
	if( !( out->m_list = NLib_Memoire_NListe_Construire( (void ( ___cdecl* )( void* ))NParser_Output_NParserOutput_Destroy ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Free
		NFREE( out );

		// Quit
		return NULL;
	}

	// OK
	return out;
}

/**
 * Duplicate output list
 *
 * @param src
 * 		The original output list
 *
 * @return the output list instance
 */
__ALLOC NParserOutputList *NParser_Output_NParserOutputList_Build2( const NParserOutputList *src )
{
	// Output
	NParserOutputList *out;

	// Iterator
	NU32 i;

	// Entry
	NParserOutput *entry;

	// Allocate memory
	if( !( out = NParser_Output_NParserOutputList_Build( ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Quit
		return NULL;
	}

	// Duplicate entries
	for( i = 0; i < NLib_Memoire_NListe_ObtenirNombre( src->m_list ); i++ )
	{
		// Duplicate entry
		if( !( entry = NParser_Output_NParserOutput_Build2( NLib_Memoire_NListe_ObtenirElementDepuisIndex( src->m_list,
			i ) ) ) )
		{
			// Notify
			NOTIFIER_ERREUR( NERREUR_COPY );

			// Destroy list
			NLib_Memoire_NListe_Detruire( &out->m_list );

			// Free
			NFREE( out );

			// Quit
			return NULL;
		}

		// Add entry
		if( !NLib_Memoire_NListe_Ajouter( out->m_list,
			entry ) )
		{
			// Notify
			NOTIFIER_ERREUR( NERREUR_EXPORT_FAILED );

			// Destroy list
			NLib_Memoire_NListe_Detruire( &out->m_list );

			// Free
			NFREE( out );

			// Quit
			return NULL;
		}
	}

	// OK
	return out;
}

/**
 * Destroy output list
 *
 * @param this
 * 		This instance
 */
void NParser_Output_NParserOutputList_Destroy( NParserOutputList **this )
{
	// Destroy list
	NLib_Memoire_NListe_Detruire( &(*this)->m_list );

	// Free
	NFREE( (*this) );
}

/**
 * Add an entry
 *
 * @param this
 * 		This instance
 * @param key
 * 		The key
 * @param value
 * 		The value
 * @param type
 * 		The value type
 *
 * @return if operation succeeded
 */
NBOOL NParser_Output_NParserOutputList_AddEntry( NParserOutputList *this,
	const char *key,
	__WILLBEOWNED char *value,
	NParserOutputType type )
{
	// Entry
	struct NParserOutput *entry;

	// Create entry
	if( !( entry = NParser_Output_NParserOutput_Build( key,
		value,
		type ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Quit
		return NFALSE;
	}

	// Add
	return NParser_Output_NParserOutputList_AddEntry2( this,
		entry );
}

/**
 * Add an entry
 *
 * @param this
 * 		This instance
 * @param entry
 * 		The entry to add
 *
 * @return if operation succeeded
 */
NBOOL NParser_Output_NParserOutputList_AddEntry2( NParserOutputList *this,
	__WILLBEOWNED NParserOutput *entry )
{
	// Add
	return NLib_Memoire_NListe_Ajouter( this->m_list,
		entry );
}

/**
 * Add an entry duplicate
 *
 * @param this
 * 		This instance
 * @param entry
 * 		The entry source to duplicate and to add
 *
 * @return if operation succeeded
 */
NBOOL NParser_Output_NParserOutputList_AddEntry3( NParserOutputList *this,
	const NParserOutput *entry )
{
	// Parser entry duplicate
	NParserOutput *entryDuplicate;

	// Duplicate entry
	if( !( entryDuplicate = NParser_Output_NParserOutput_Build2( entry ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_COPY );

		// Quit
		return NFALSE;
	}

	// Add entry
	return NParser_Output_NParserOutputList_AddEntry2( this,
		entryDuplicate );
}

/**
 * Add integer entry
 *
 * @param this
 * 		This instance
 * @param key
 * 		The key
 * @param value
 * 		The value
 *
 * @return if operation succeeded
 */
NBOOL NParser_Output_NParserOutputList_AddEntryInteger( NParserOutputList *this,
	const char *key,
	NU32 value )
{
	// Value copy
	NU32 *valueCopy;

	// Duplicate
	if( !( valueCopy = calloc( 1,
		sizeof( NU32 ) ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_COPY );

		// Quit
		return NFALSE;
	}

	// Copy value
	*valueCopy = value;

	// Add entry
	return NParser_Output_NParserOutputList_AddEntry( this,
		key,
		(char*)valueCopy,
		NPARSER_OUTPUT_TYPE_INTEGER );
}

/**
 * Add boolean value
 *
 * @param this
 * 		This instance
 * @param key
 * 		The key
 * @param value
 * 		The value
 *
 * @return if operation succeeded
 */
NBOOL NParser_Output_NParserOutputList_AddEntryBoolean( NParserOutputList *this,
	const char *key,
	NBOOL value )
{
	// Value copy
	NBOOL *valueCopy;

	// Duplicate
	if( !( valueCopy = calloc( 1,
		sizeof( NBOOL ) ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_COPY );

		// Quit
		return NFALSE;
	}

	// Copy value
	*valueCopy = value;

	// Add entry
	return NParser_Output_NParserOutputList_AddEntry( this,
		key,
		(char*)valueCopy,
		NPARSER_OUTPUT_TYPE_BOOLEAN );
}

/**
 * Add double value
 *
 * @param this
 * 		This instance
 * @param key
 * 		The key
 * @param value
 * 		The value
 *
 * @return if operation succeeded
 */
NBOOL NParser_Output_NParserOutputList_AddEntryDouble( NParserOutputList *this,
	const char *key,
	double value )
{
	// Value copy
	double *valueCopy;

	// Duplicate
	if( !( valueCopy = calloc( 1,
		sizeof( double ) ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_COPY );

		// Quit
		return NFALSE;
	}

	// Copy value
	*valueCopy = value;

	// Add entry
	return NParser_Output_NParserOutputList_AddEntry( this,
		key,
		(char*)valueCopy,
		NPARSER_OUTPUT_TYPE_DOUBLE );
}

/**
 * Add string value
 *
 * @param this
 * 		This instance
 * @param key
 * 		The key
 * @param value
 * 		The value
 *
 * @return if operations succeeded
 */
NBOOL NParser_Output_NParserOutputList_AddEntryString( NParserOutputList *this,
	const char *key,
	const char *value )
{
	// Value copy
	char *valueCopy;

	// Check value
	if( value == NULL )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_PARAMETER_ERROR );

		// Quit
		return NFALSE;
	}

	// Duplicate
	if( !( valueCopy = NLib_Chaine_Dupliquer( value ) ) )
	{
		// Notify
		NOTIFIER_ERREUR( NERREUR_COPY );

		// Quit
		return NFALSE;
	}

	// Add entry
	return NParser_Output_NParserOutputList_AddEntry( this,
		key,
		valueCopy,
		NPARSER_OUTPUT_TYPE_STRING );
}

/**
 * Add all the entry from another parser output list
 *
 * @param this
 * 		This instance
 * @param parserOutputList
 * 		The other parser output list
 *
 * @return if operation succeeded
 */
NBOOL NParser_Output_NParserOutputList_AddAllEntry( NParserOutputList *this,
	NParserOutputList *parserOutputList )
{
	// Iterator
	NU32 i = 0;

	// Entry
	const NParserOutput *entry;

	// Entry copy
	NParserOutput *entryCopy;

	// Iterate
	for( ; i < NParser_Output_NParserOutputList_GetEntryCount( parserOutputList ); i++ )
	{
		// Get entry
		entry = NParser_Output_NParserOutputList_GetEntry( parserOutputList,
			i );

		// Duplicate entry
		if( !( entryCopy = NParser_Output_NParserOutput_Build2( entry ) ) )
		{
			// Notify
			NOTIFIER_ERREUR( NERREUR_COPY );

			// Quit
			return NTRUE;
		}

		// Add entry
		NParser_Output_NParserOutputList_AddEntry2( this,
			entryCopy );
	}

	// OK
	return NTRUE;
}

/**
 * Get output by the key
 *
 * @param this
 * 		This instance
 * @param key
 * 		The key to look for
 * @param isKeyCaseSensitive
 * 		Is case sensitive
 * @param isFullKey
 * 		Do we want the output key to be exactly %key%, or just to contain it?
 *
 * @return the output
 */
const NParserOutput *NParser_Output_NParserOutputList_GetFirstOutput( const NParserOutputList *this,
	const char *key,
	NBOOL isKeyCaseSensitive,
	NBOOL isFullKey )
{
	// Iterator
	NU32 i = 0;

	// Element
	const NParserOutput *element;

	// Look for
	for( ; i < NLib_Memoire_NListe_ObtenirNombre( this->m_list ); i++ )
	{
		// Get element
		element = NLib_Memoire_NListe_ObtenirElementDepuisIndex( this->m_list,
			i );

		// Check key
		if( isFullKey ?
			NLib_Chaine_Comparer( NParser_Output_NParserOutput_GetKey( element ),
				key,
				isKeyCaseSensitive,
				0 )
				: NLib_Chaine_EstChaineContient2( NParser_Output_NParserOutput_GetKey( element ),
					key,
					isKeyCaseSensitive ) )
			// Found
			return NLib_Memoire_NListe_ObtenirElementDepuisIndex( this->m_list,
				i );
	}

	// Not found
	return NULL;
}

/**
 * Get output list by the key
 *
 * @param this
 * 		This instance
 * @param key
 * 		The key to look for
 * @param isKeyCaseSensitive
 * 		Is case sensitive
 * @param isFullKey
 * 		Do we want the output key to be exactly %key%, or just to contain it?
 *
 * @return the output list
 */
__ALLOC NParserOutputList *NParser_Output_NParserOutputList_GetAllOutput( const NParserOutputList *this,
	const char *key,
	NBOOL isKeyCaseSensitive,
	NBOOL isFullKey )
{
	// Iterator
	NU32 i = 0;

	// Output
	__OUTPUT NParserOutputList *out;

	// Element
	const NParserOutput *element;

	// Create list
	if( !( out = NParser_Output_NParserOutputList_Build( ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Quit
		return NULL;
	}

	// Look for
	for( ; i < NLib_Memoire_NListe_ObtenirNombre( this->m_list ); i++ )
	{
		// Get element
		element = NLib_Memoire_NListe_ObtenirElementDepuisIndex( this->m_list,
			i );

		// Check key
		if( isFullKey ?
			NLib_Chaine_Comparer( NParser_Output_NParserOutput_GetKey( element ),
				key,
				isKeyCaseSensitive,
				0 )
			: NLib_Chaine_EstChaineContient2( NParser_Output_NParserOutput_GetKey( element ),
				key,
				isKeyCaseSensitive ) )
			// Add to list
			NParser_Output_NParserOutputList_AddEntry( out,
				NParser_Output_NParserOutput_GetKey( element ),
				NParser_Output_NParserOutput_GetValueCopy( element ),
				NParser_Output_NParserOutput_GetType( element ) );
	}

	// OK
	return out;
}

/**
 * Display all output
 *
 * @param this
 * 		This instance
 */
void NParser_Output_NParserOutputList_Display( const NParserOutputList *this )
{
	// Iterator
	NU32 i = 0;

	// Parser output element
	const NParserOutput *element;

	// Display items count
	printf( "There are %d elements:\n\n",
		NLib_Memoire_NListe_ObtenirNombre( this->m_list ) );

	// Iterate results
	for( ; i < NLib_Memoire_NListe_ObtenirNombre( this->m_list ); i++ )
	{
		// Get element
		if( !( element = NLib_Memoire_NListe_ObtenirElementDepuisIndex( this->m_list,
			i ) ) )
			continue;

		// Display
		NParser_Output_NParserOutput_Display( element );
	}
}

/**
 * Get entry count
 *
 * @param this
 * 		This instance
 *
 * @return the entry count
 */
NU32 NParser_Output_NParserOutputList_GetEntryCount( const NParserOutputList *this )
{
	return NLib_Memoire_NListe_ObtenirNombre( this->m_list );
}

/**
 * Get entry by index
 *
 * @param this
 * 		This instance
 * @param index
 * 		The index
 *
 * @return the entry
 */
const NParserOutput *NParser_Output_NParserOutputList_GetEntry( const NParserOutputList *this,
	NU32 index )
{
	// Check
	if( index >= NLib_Memoire_NListe_ObtenirNombre( this->m_list ) )
		// Error
		return NULL;

	// OK
	return NLib_Memoire_NListe_ObtenirElementDepuisIndex( this->m_list,
		index );
}

/**
 * Remove an entry
 *
 * @param this
 * 		This instance
 * @param index
 * 		The index of entry to remove
 *
 * @return if operation succeeded
 */
NBOOL NParser_Output_NParserOutputList_RemoveEntry( NParserOutputList *this,
	NU32 index )
{
	return NLib_Memoire_NListe_SupprimerDepuisIndex( this->m_list,
		index );
}

/**
 * Count key occurence
 *
 * @param this
 * 		This instance
 * @param key
 * 		The key to find
 * @param isStartingWith
 * 		Do we check for the key beginning or for all the key
 *
 * @return the occurence count
 */
NBOOL NParser_Output_NParserOutputList_CountKeyOccurence( const NParserOutputList *this,
	const char *key,
	NBOOL isStartingWith )
{
	// Iterator
	NU32 i = 0;

	// Output
	__OUTPUT NU32 out = 0;

	// Count
	for( ; i < NLib_Memoire_NListe_ObtenirNombre( this->m_list ); i++ )
		// Checking beginning?
		if( isStartingWith )
		{
			// Check beginning
			if( NLib_Chaine_EstChaineCommencePar( NParser_Output_NParserOutput_GetKey( NLib_Memoire_NListe_ObtenirElementDepuisIndex( this->m_list,
				i ) ),
				key ) )
				// Increment
				out++;
		}
		// Checking whole key
		else
			// Same key?
			if( NLib_Chaine_Comparer( NParser_Output_NParserOutput_GetKey( NLib_Memoire_NListe_ObtenirElementDepuisIndex( this->m_list,
					i ) ),
				key,
				NTRUE,
				0 ) )
				// Increment
				out++;

	// OK
	return out;
}

