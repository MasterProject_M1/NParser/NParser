#include "../include/NParser.h"

// -----------------
// namespace NParser
// -----------------

#ifdef NPARSER_EASY_FUNCTION
/**
 * Parse file
 *
 * @param filepath
 * 		The file path
 * @param type
 * 		The file type
 *
 * @return the parsed result
 */
__ALLOC NParserOutputList *NParser_ParseFile( const char *filepath,
	NParserType type )
{
	switch( type )
	{
		case NPARSER_TYPE_JSON:
			return NJson_Engine_Parser_Parse2( filepath );
		case NPARSER_TYPE_YAML:
			return NYaml_Output_Parse2( filepath );

		default:
			// Notify
			NOTIFIER_ERREUR( NERREUR_UNIMPLEMENTED );

			// Quit
			return NULL;
	}
}

/**
 * Parse text
 *
 * @param text
 * 		The text
 * @param type
 * 		The text type
 *
 * @return the parsed result
 */
__ALLOC NParserOutputList *NParser_ParseText( const char *text,
	NParserType type )
{
	switch( type )
	{
		case NPARSER_TYPE_JSON:
			return NJson_Engine_Parser_Parse( text,
				(NU32)strlen( text ) );

		case NPARSER_TYPE_YAML:
			return NYaml_Output_Parse( text );

		default:
			// Notify
			NOTIFIER_ERREUR( NERREUR_UNIMPLEMENTED );

			// Quit
			return NULL;
	}
}
#endif // NPARSER_EASY_FUNCTION
