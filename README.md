NParser
=======

Introduction
------------

The common data model for json and yaml parser output

Dependencies
------------

This project depends on

- NLib

If NPARSER_EASY_FUNCTION is defined:

- NYaml
- NJson

Author
------

SOARES Lucas <lucas.soares.npro@gmail.com>

https://gitlab.com/MasterProject_M1/NParser/
